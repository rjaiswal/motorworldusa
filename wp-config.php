<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'car_for_you');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W-(mcw[3E[kj&QaS<~T7@hD7I86IwX6BlCs|?E}Wl$k<8M<Z~6Gv&ftO[l!qt.0+');
define('SECURE_AUTH_KEY',  '[zP|G+8k,ou123O^l):^@1U59U.pUyPEELwE{M!vC+:rJdr-rDS|~o-LQhT)Y</@');
define('LOGGED_IN_KEY',    '~ ~TT/}19.Y)>2q^[.ME3 <v-NSp:#rz[-zg_I}^0P5LK8Il}<h|{k1d})kQm#Kl');
define('NONCE_KEY',        ')@Xg7{Ix&(myzN?6tE/M^{l2tNx#e!v_tj+pdzabuSM.@/6f1NRJ3.9=@[?[Q4|r');
define('AUTH_SALT',        '!eo.-0|pqv4T (Spy,&kiQ}Xp,ACeE#cQ=?:(uK>XV #^IO0kNU*(r#k]$^=;w[!');
define('SECURE_AUTH_SALT', 'xOD]8oD9,L!7h>O.1$JU&As4P9GZFbZ+X=bRT0 iC|x]ZT&z:[=Vaz@JXO<$ueEL');
define('LOGGED_IN_SALT',   'EuKX!%1-Cr%/2osD9Smy1N48I+{q2S2;[Il3)3onkCH9~6); DhvuxAnZ(1a5P={');
define('NONCE_SALT',       'v|ene[79kLQ;ptpH$_],S?%P3b^:Y]ATKU6^q22@nbTY2O[I*+eO4&sOJZ6a G.D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
