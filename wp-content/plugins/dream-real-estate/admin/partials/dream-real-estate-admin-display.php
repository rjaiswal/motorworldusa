<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://themeforest.net/user/DreamThemes
 * @since      1.0.0
 *
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
