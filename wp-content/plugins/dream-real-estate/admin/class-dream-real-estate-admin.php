<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://themeforest.net/user/DreamThemes

 * @since      1.0.0
 *
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/admin
 */
/**

 * The admin-specific functionality of the plugin.

 *

 * Defines the plugin name, version, and two examples hooks for how to

 * enqueue the admin-specific stylesheet and JavaScript.

 *

 * @package    Dream_Real_Estate

 * @subpackage Dream_Real_Estate/admin

 * @author     M Saqib Sarwar <saqib@dreamthemes.com>

 */

class Dream_Real_Estate_Admin {



	/**

	 * The ID of this plugin.

	 *

	 * @since    1.0.0

	 * @access   private

	 * @var      string    $plugin_name    The ID of this plugin.

	 */

	private $plugin_name;



	/**

	 * The version of this plugin.

	 *

	 * @since    1.0.0

	 * @access   private

	 * @var      string    $version    The current version of this plugin.

	 */

	private $version;



    /**

     * Real estate options

     *

     * @since    1.0.0

     * @access   public

     * @var      array    $options    Contains the plugin options

     */

    public $options;



	/**

	 * Initialize the class and set its properties.

	 *

	 * @since    1.0.0

	 * @param      string    $plugin_name       The name of this plugin.

	 * @param      string    $version    The version of this plugin.

	 */

	public function __construct( $plugin_name, $version ) {



		$this->plugin_name = $plugin_name;

		$this->version = $version;

        $this->options = get_option( 'dream_price_format_option' );
	}
	/**

	 * Register the stylesheets for the admin area.

	 *

	 * @since    1.0.0
	 */
	public function enqueue_styles() {
        if( $this::is_auto_edit_page() ) {

            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/dream-real-estate-admin.css', array(), $this->version, 'all');

        }



	}



	/**

	 * Register the JavaScript for the admin area.

	 *

	 * @since    1.0.0

	 */

	public function enqueue_scripts() {
        if( $this::is_auto_edit_page() ) {

            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/dream-real-estate-admin.js', array('jquery', 'jquery-ui-sortable'), $this->version, false);

        }
    }
    /**

     * Check if it is a auto edit page.

     * @return bool

     */

    public static function is_auto_edit_page(){

        if ( is_admin() ) {

            global $pagenow;

            if ( in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) {

                global $post_type;

                if ( 'auto' == $post_type ) {

                    return true;

                }

            }

        }

        return false;

    }
    /**

     * Add plugin settings page

     * @since   1.0.0

     */

    public function add_real_estate_settings(){
        add_plugins_page(

            __( 'Dream Real Estate Settings', 'dream-real-estate'),

            __( 'Dream Real Estate', 'dream-real-estate'),

            'administrator',

            'dream_real_estate',

            array( $this, 'display_real_estate_settings')
        );
    }
    /**

     * Display real estate settings page

     * @since   1.0.0

     */

    public function display_real_estate_settings() {
        ?>
        <!-- Create a header in the default WordPress 'wrap' container -->
        <div class="wrap">
            <h2><?php _e( 'Dream Real Estate Settings', 'dream-real-estate' ); ?></h2>
            <!-- Make a call to the WordPress function for rendering errors when settings are saved. -->
            <?php settings_errors(); ?>
            <!-- Create the form that will be used to render our options -->
            <form method="post" action="options.php">
                <?php settings_fields( 'dream_real_estate_options_group' ); ?>
                <?php do_settings_sections( 'dream_price_format_page' ); ?>
                <?php submit_button(); ?>
            </form>
        </div><!-- /.wrap -->
        <?php
    }
    /**
     * Initialize real estate settings page
     */
    public function initialize_real_estate_options(){
        // create plugin options if not exist
        if( false == $this->options ) {
            add_option( 'dream_price_format_option' );
        }
        /**

         * Section

         */

        add_settings_section(

            'dream_price_format_section',                                                 // ID used to identify this section and with which to register options

            __( 'Price Format', 'dream-real-estate'),                                     // Title to be displayed on the administration page

            array( $this, 'dream_price_format_settings_description'),                     // Callback used to render the description of the section

            'dream_price_format_page'                                                     // Page on which to add this section of options

        );



        /**

         * Fields

         */

        add_settings_field(

            'currency_sign',
            __( 'Currency Sign', 'dream-real-estate' ),
            array( $this, 'dream_text_option_field' ),
            'dream_price_format_page',
            'dream_price_format_section',
            array(
                'field_id'        => 'currency_sign',
                'field_option'    => 'dream_price_format_option',
                'field_default'   => '$',
            )
        );
        add_settings_field(

            'currency_position',

            __( 'Currency Sign Position', 'dream-real-estate' ),

            array( $this, 'dream_select_option_field' ),

            'dream_price_format_page',

            'dream_price_format_section',

            array (
                'field_id'          => 'currency_position',
                'field_option'      => 'dream_price_format_option',
                'field_default'     => 'before',
                'field_options'     => array(
                    'before'   => __( 'Before ($450,000)', 'dream-real-estate' ),
                    'after'    => __( 'After (450,000$)', 'dream-real-estate' ),
                )
            )
        );



        add_settings_field(

            'thousand_separator',

            __( 'Thousand Separator', 'dream-real-estate' ),

            array( $this, 'dream_text_option_field' ),

            'dream_price_format_page',

            'dream_price_format_section',

            array(

                'field_id'        => 'thousand_separator',

                'field_option'    => 'dream_price_format_option',

                'field_default'   => ',',

            )

        );



        add_settings_field(

            'decimal_separator',

            __( 'Decimal Separator', 'dream-real-estate' ),

            array( $this, 'dream_text_option_field' ),

            'dream_price_format_page',

            'dream_price_format_section',

            array(

                'field_id'        => 'decimal_separator',

                'field_option'    => 'dream_price_format_option',

                'field_default'   => '.',

            )

        );



        add_settings_field(

            'number_of_decimals',

            __( 'Number of Decimals', 'dream-real-estate' ),

            array( $this, 'dream_text_option_field' ),

            'dream_price_format_page',

            'dream_price_format_section',

            array(

                'field_id'        => 'number_of_decimals',

                'field_option'    => 'dream_price_format_option',

                'field_default'   => '0',

            )

        );



        add_settings_field(

            'empty_price_text',

            __( 'Empty Price Text', 'dream-real-estate' ),

            array( $this, 'dream_text_option_field' ),

            'dream_price_format_page',

            'dream_price_format_section',

            array(

                'field_id'          => 'empty_price_text',

                'field_option'      => 'dream_price_format_option',

                'field_default'     => __( 'Price on call', 'dream-real-estate' ),

                'field_description' => __( 'Text to display in case of empty price. Example: Price on call', 'dream-real-estate' ),

            )

        );





        /**

         * Register Settings

         */

        register_setting( 'dream_real_estate_options_group', 'dream_price_format_option' );

    }



    /**

     * Price format section description

     */

    public function dream_price_format_settings_description() {

        echo '<p>'. __( 'You can modify price format to suit your needs, Using options provided below.', 'dream-real-estate' ) . '</p>';

    }





    /**

     * Reusable text option field for settings page

     *

     * @param $args array   field arguments

     */

    public function dream_text_option_field( $args ) {

        extract( $args );

        if( $field_id ) {

            $field_value = ( isset( $this->options[ $field_id ] ) ) ? $this->options[ $field_id ] : $field_default;

            echo '<input name="' . $field_option . '[' . $field_id . ']" class="dream-text-field '. $field_id .'" value="' . $field_value . '" />';

            if ( isset( $field_description ) ) {

                echo '<br/><label class="dream-field-description">' . $field_description . '</label>';

            }

        } else {

            _e( 'Field id is missing!', 'dream-real-estate' );

        }

    }





    /**

     * Reusable select options field for settings page

     *

     * @param $args array   field arguments

     */

    public function dream_select_option_field( $args ) {

        extract( $args );

        if( $field_id ) {

            $existing_value = ( isset( $this->options[ $field_id ] ) ) ? $this->options[ $field_id ] : $field_default;

            ?>

            <select name="<?php echo $field_option . '[' . $field_id . ']'; ?>" class="dream-select-field <?php echo $field_id; ?>">

                <?php foreach( $field_options as $key => $value ) { ?>

                    <option value="<?php echo $key; ?>" <?php selected( $existing_value, $key ); ?>><?php echo $value; ?></option>

                <?php } ?>

            </select>

            <?php

            if ( isset( $field_description ) ) {

                echo '<br/><label class="dream-field-description">' . $field_description . '</label>';

            }

        } else {

            _e( 'Field id is missing!', 'dream-real-estate' );

        }

    }



    /**

     * Add plugin action links

     *

     * @param $links

     * @return array

     */

    public function dream_real_estate_action_links( $links ) {

        $links[] = '<a href="'. get_admin_url( null, 'plugins.php?page=dream_real_estate' ) .'">' . __( 'Settings', 'dream-real-estate' ) . '</a>';

        return $links;

    }



}

