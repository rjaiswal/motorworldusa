<?php
/**
 * Auto custom post type class.
 *
 * Defines the auto post type.
 *
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/admin
 * @author     M Saqib Sarwar <saqib@dreamthemes.com>
 */
class Dream_Auto_Post_Type {
    /**
     * Register Auto Post Type
     * @since 1.0.0
     */
    public function register_auto_post_type() {
        $labels = array(
            'name'                => _x( 'Autodeal', 'Post Type General Name', 'dream-real-estate' ),
            'singular_name'       => _x( 'Auto', 'Post Type Singular Name', 'dream-real-estate' ),
            'menu_name'           => __( 'Autodeal', 'dream-real-estate' ),
            'name_admin_bar'      => __( 'Auto', 'dream-real-estate' ),
            'parent_item_colon'   => __( 'Parent Auto:', 'dream-real-estate' ),
            'all_items'           => __( 'All Autodeal', 'dream-real-estate' ),
            'add_new_item'        => __( 'Add New Auto', 'dream-real-estate' ),
            'add_new'             => __( 'Add New', 'dream-real-estate' ),
            'new_item'            => __( 'New Auto', 'dream-real-estate' ),
            'edit_item'           => __( 'Edit Auto', 'dream-real-estate' ),
            'update_item'         => __( 'Update Auto', 'dream-real-estate' ),
            'view_item'           => __( 'View Auto', 'dream-real-estate' ),
            'search_items'        => __( 'Search Auto', 'dream-real-estate' ),
            'not_found'           => __( 'Not found', 'dream-real-estate' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'dream-real-estate' ),
        );
        $rewrite = array(
            'slug'                => __( 'auto', 'dream-real-estate' ),
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );

        $args = array(
            'label'               => __( 'auto', 'dream-real-estate' ),
            'description'         => __( 'Real Estate Auto', 'dream-real-estate' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes', 'comments' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'auto', $args );
    }
    /**
     * Register Auto Brand Taxonomy
     * @since 1.0.0
     */
    public function register_auto_brand_taxonomy() {
        $labels = array(
            'name'                       => _x( 'Auto Brand', 'Taxonomy General Name', 'dream-real-estate' ),
            'singular_name'              => _x( 'Auto Brand', 'Taxonomy Singular Name', 'dream-real-estate' ),
            'menu_name'                  => __( 'Brands', 'dream-real-estate' ),
            'all_items'                  => __( 'All Auto Brands', 'dream-real-estate' ),
            'parent_item'                => __( 'Parent Auto Brand', 'dream-real-estate' ),
            'parent_item_colon'          => __( 'Parent Auto Brand:', 'dream-real-estate' ),
            'new_item_name'              => __( 'New Auto Brand Name', 'dream-real-estate' ),
            'add_new_item'               => __( 'Add New Auto Brand', 'dream-real-estate' ),
            'edit_item'                  => __( 'Edit Auto Brand', 'dream-real-estate' ),
            'update_item'                => __( 'Update Auto Brand', 'dream-real-estate' ),
            'view_item'                  => __( 'View Auto Brand', 'dream-real-estate' ),
            'separate_items_with_commas' => __( 'Separate Auto Brands with commas', 'dream-real-estate' ),
            'add_or_remove_items'        => __( 'Add or remove Auto Brands', 'dream-real-estate' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'dream-real-estate' ),
            'popular_items'              => __( 'Popular Auto Brands', 'dream-real-estate' ),
            'search_items'               => __( 'Search Auto Brands', 'dream-real-estate' ),
            'not_found'                  => __( 'Not Found', 'dream-real-estate' ),
        );
        $rewrite = array(
            'slug'                       => __( 'auto-brand', 'dream-real-estate' ),
            'with_front'                 => true,
            'hierarchical'               => true,
        );
        $args = array(

            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'auto-brand', array( 'auto' ), $args );
    }
    /**
     * Register Auto Location Taxonomy
     * @since 1.0.0
     */
    public function register_auto_location_taxonomy() {
        $labels = array(
            'name'                       => _x( 'Auto Location', 'Taxonomy General Name', 'dream-real-estate' ),
            'singular_name'              => _x( 'Auto Status', 'Taxonomy Singular Name', 'dream-real-estate' ),
            'menu_name'                  => __( 'Locations', 'dream-real-estate' ),
            'all_items'                  => __( 'All Auto Locations', 'dream-real-estate' ),
            'parent_item'                => __( 'Parent Auto Location', 'dream-real-estate' ),
            'parent_item_colon'          => __( 'Parent Auto Location:', 'dream-real-estate' ),
            'new_item_name'              => __( 'New Auto Location Name', 'dream-real-estate' ),
            'add_new_item'               => __( 'Add New Auto Location', 'dream-real-estate' ),
            'edit_item'                  => __( 'Edit Auto Location', 'dream-real-estate' ),
            'update_item'                => __( 'Update Auto Location', 'dream-real-estate' ),
            'view_item'                  => __( 'View Auto Location', 'dream-real-estate' ),
            'separate_items_with_commas' => __( 'Separate Auto Locations with commas', 'dream-real-estate' ),
            'add_or_remove_items'        => __( 'Add or remove Auto Locations', 'dream-real-estate' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'dream-real-estate' ),
            'popular_items'              => __( 'Popular Auto Locations', 'dream-real-estate' ),
            'search_items'               => __( 'Search Auto Locations', 'dream-real-estate' ),
            'not_found'                  => __( 'Not Found', 'dream-real-estate' ),
        );
        $rewrite = array(
            'slug'                       => __( 'auto-location', 'dream-real-estate' ),
            'with_front'                 => true,
            'hierarchical'               => true,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'auto-location', array( 'auto' ), $args );
    }
    /**
     * Register Auto City Taxonomy
     * @since 1.0.0
     */
    public function register_year_model_taxonomy() {
        $labels = array(
            'name'                       => _x( 'Year of Model ', 'Taxonomy General Name', 'dream-real-estate' ),
            'singular_name'              => _x( 'Year of Model ', 'Taxonomy Singular Name', 'dream-real-estate' ),
            'menu_name'                  => __( 'Year of Model', 'dream-real-estate' ),
            'all_items'                  => __( 'All Year of Models', 'dream-real-estate' ),
            'parent_item'                => __( 'Parent Year of Model ', 'dream-real-estate' ),
            'parent_item_colon'          => __( 'Parent Year of Model :', 'dream-real-estate' ),
            'new_item_name'              => __( 'New Year of Model Name', 'dream-real-estate' ),
            'add_new_item'               => __( 'Add New Year of Model', 'dream-real-estate' ),
            'edit_item'                  => __( 'Edit Year of Model', 'dream-real-estate' ),
            'update_item'                => __( 'Update Year of Model', 'dream-real-estate' ),
            'view_item'                  => __( 'View Year of Model', 'dream-real-estate' ),
            'separate_items_with_commas' => __( 'Separate Year of Models with commas', 'dream-real-estate' ),
            'add_or_remove_items'        => __( 'Add or remove Year of Models', 'dream-real-estate' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'dream-real-estate' ),
            'popular_items'              => __( 'Popular Year of Models', 'dream-real-estate' ),
            'search_items'               => __( 'Search Year of Models', 'dream-real-estate' ),
            'not_found'                  => __( 'Not Found', 'dream-real-estate' ),
        );
        $rewrite = array(
            'slug'                       => __( 'auto-year-model', 'dream-real-estate' ),
            'with_front'                 => true,
            'hierarchical'               => true,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'year-model', array( 'auto' ), $args );
    }
    /**
     * Register custom columns
     *
     * @param   array   $defaults
     * @since   1.0.0
     * @return  array   $defaults
     */
    public function register_custom_column_titles ( $defaults ) {
        $new_columns = array(
            //"thumb"     => __( 'Photo', 'dream-real-estate' ),
            //"id"        => __( 'Custom ID', 'dream-real-estate' ),
            "price"     => __( 'Price', 'dream-real-estate'),
        );
        $last_columns = array();
        if ( count( $defaults ) > 5 ) {
            unset( $defaults['author'] );
            $last_columns = array_splice( $defaults, 4, 5 );
            // Simplify column titles
        }
        $defaults = array_merge( $defaults, $new_columns );
        $defaults = array_merge( $defaults, $last_columns );
        return $defaults;
    }
    /**
     * Display custom column for properties
     *
     * @access  public
     * @param   string $column_name
     * @since   1.0.0
     * @return  void
     */
    public function display_custom_column ( $column_name ) {
        global $post;
        switch ( $column_name ) {
            case 'price':
                $auto_price = get_post_meta ( $post->ID, 'DREAM_auto_price', true );
                if ( !empty ( $auto_price ) ) {
                    $price_amount = doubleval( $auto_price );
                    $price_postfix = get_post_meta ( $post->ID, 'DREAM_auto_price_postfix', true );
                    echo Dream_Auto::format_price( $price_amount, $price_postfix );
                } else {
                    _e ( 'NA', 'dream-real-estate' );
                }
                break;
            default:
                break;
        }
    }
    /**
     * Register meta boxes related to auto post type
     *
     * @param   array   $meta_boxes
     * @since   1.0.0
     * @return  array   $meta_boxes
     */
    public function register_meta_boxes ( $meta_boxes ){
        $prefix = 'DREAM_';
        // Agents

        $agents_array = array( -1 => __( 'None', 'dream-real-estate' ) );

        $agents_posts = get_posts( array (

            'post_type' => 'agent',

            'posts_per_page' => -1,

            'suppress_filters' => 0,

            ) );

        if ( ! empty ( $agents_posts ) ) {

            foreach ( $agents_posts as $agent_post ) {

                $agents_array[ $agent_post->ID ] = $agent_post->post_title;
            }
        }

        // Auto Details Meta Box
        $default_desc = __( 'Consult theme documentation for required image size.', 'dream-real-estate' );
        $gallery_images_desc = apply_filters( 'dream_gallery_description', $default_desc );
        $video_image_desc = apply_filters( 'dream_video_description', $default_desc );
        $slider_image_desc = apply_filters( 'dream_slider_description', $default_desc );
        $meta_boxes[] = array(
            'id' => 'auto-meta-box',
            'title' => __('Auto', 'dream-real-estate'),
            'pages' => array('auto'),
            'tabs' => array(
                'details' => array(
                    'label' => __('Basic Information', 'dream-real-estate'),
                    'icon' => 'dashicons-admin-home',
                ),
				
//                'agent' => array(
//
//                    'label' => __('Agent Information', 'dream-real-estate'),
//
//                    'icon' => 'dashicons-businessman',
//
//                ),				
                'featured_auto' => array(
                    'label' => __('Technical Specification', 'dream-real-estate'),
                    'icon' => 'dashicons-format-image',
                ),
                'add_fea_pro' => array(
                    'label' => __('Accessories', 'dream-real-estate'),
                    'icon' => 'dashicons-format-image',
                ),				
                'gallery' => array(
                    'label' => __('Slider Images', 'dream-real-estate'),
                    'icon' => 'dashicons-format-gallery',
                ),			
                'featured_car' => array(
                    'label' => __('Featured Auto', 'dream-real-estate'),
                    'icon' => 'dashicons-format-image',
                ),
            ),
            'tab_style' => 'left',
            'fields' => array(
                // Details
                array(

                    'id' => "{$prefix}auto_price",
                    'name' => __('Sale Price ( Only digits )', 'dream-real-estate'),
                    'desc' => __('Example Value: 435000', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),

                array(
                    'id' => "{$prefix}auto_body_color",
                    'name' => __('Body Color', 'dream-real-estate'),
                    'desc' => __('Example Value: Silver, White, Black', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),

                array(
                    'id' => "{$prefix}auto_id",
                    'name' => __('Auto ID', 'dream-real-estate'),
                    'desc' => __('It will help you search a auto directly.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),

				array(
					'id' => "{$prefix}auto_condition",
					'name' => __('Vehicle Condition', 'dream-real-estate'),
					'desc' => __('Select Vehicle Condition Example: new and used.', 'dream-real-estate'),
					'tab' => 'details',
					'type'		=> 'select',
					'columns' => 6,
					'options'	=> array(
						'used'=> 'Used ',
						'new' => 'New',
					),
					'multiple'	=> false,
				),			
                // Map
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'google_map_divider', // Not used, but needed
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}auto_address",
                    'name' => __('Auto Address', 'dream-real-estate'),
                    'desc' => __('Leaving it empty will hide the google map on auto detail page.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => ' 28, Academic District, Moscow',
                    'columns' => 12,
                    'tab' => 'details',
                ),

//                array(
//
//                    'id' => "{$prefix}auto_location",
//
//                    'name' => __('Auto Location at Google Map*', 'dream-real-estate'),
//
//                    'desc' => __('Drag the google map marker to point your auto location. You can also use the address field above to search for your auto.', 'dream-real-estate'),
//
//                    'type' => 'map',
//
//                    'std' => '26.011812,-80.14524499999999,15',   // 'latitude,longitude[,zoom]' (zoom is optional)
//
//                    'style' => 'width: 95%; height: 400px',
//
//                    'address_field' => "{$prefix}auto_address",
//
//                    'columns' => 12,
//
//                    'tab' => 'details',
//
//                ),



                array(
                    'name' => __('Auto Slider Images', 'dream-real-estate'),
                    'id' => "{$prefix}auto_slider",
                    'desc' => __('Consult theme documentation for required image size. NOT:- Please Deactivate One Click Demo Plugin after Multiple image uploade', 'dream-real-estate'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 20,
                    'columns' => 12,
                    'tab' => 'gallery',
                ),


                // Agents

//                array(
//
//                    'name' => __('What to display in agent information box ?', 'dream-real-estate'),
//
//                    'id' => "{$prefix}agent_display_option",
//
//                    'type' => 'radio',
//
//                    'std' => 'none',
//
//                    'options' => array(
//
//                        'my_profile_info' => __('Author information.', 'dream-real-estate'),
//
//                        'agent_info' => __('Agent Information. ( Select the agent below )', 'dream-real-estate'),
//
//                        'none' => __('None. ( Hide information box )', 'dream-real-estate'),
//
//                    ),
//
//                    'columns' => 12,
//
//                    'tab' => 'agent',
//
//                ),
//
//                array(
//
//                    'name' => __('Agent', 'dream-real-estate'),
//
//                    'id' => "{$prefix}agents",
//
//                    'type' => 'select',
//
//                    'options' => $agents_array,
//
//                    'columns' => 12,
//
//                    'tab' => 'agent',
//
//                ),
				
			//Featured Autodeal 	
                array(

                    'name' => __('Do you want to add this auto in Featured Cars ?', 'dream-real-estate'),
                    'id' => "{$prefix}featured_auto",
                    'type' => 'radio',
                    'std' => 'no',
                    'options' => array(
                        'yes' => __('Yes ', 'dream-real-estate'),
                        'no' => __('No', 'dream-real-estate')
                    ),
                    'columns' => 12,
                    'tab' => 'featured_car',
                ),
				
				

			//Featured Autodeal 
			
				array(
					'id' => "{$prefix}fuel_type",
					'name' => __('Vehicle Fuel Type', 'dream-real-estate'),
					'desc' => __('Select Any Example: Petrol, Diesel and Autogas.', 'dream-real-estate'),
					'tab' => 'featured_auto',
					'type'		=> 'select',
					'columns' => 6,
					'options'	=> array(
						'Diesel'=> 'Diesel',
						'Petrol' => 'Petrol',
						'Autogas' => 'Autogas',
					),
					'multiple'	=> false,
				),
				
                array(
                    'id' => "{$prefix}auto_km_done",
                    'name' => __('Kms Done (Only digits)', 'dream-real-estate'),
                    'desc' => __('Example Value: 2500', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				
                array(
                    'id' => "{$prefix}auto_transmission",
                    'name' => __('Vehicle Transmission', 'dream-real-estate'),
                    'desc' => __('Example: 4-Sports Automatic, Manual.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}auto_engine",
                    'name' => __('Engine  (Only digits)', 'dream-real-estate'),
                    'desc' => __('Example Value: 153KW', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				array(
                    'id' => "{$prefix}auto_no_of_Owners",
                    'name' => __('No. of Owners  (Only digits)', 'dream-real-estate'),
                    'desc' => __('Example Value: 1,2,3,4', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
                array(
                    'id' => "{$prefix}auto_model",
                    'name' => __('Vehicle Model', 'dream-real-estate'),
                    'desc' => __('Example:  Audi Q3, Audi A3 Cabriolet, Audi TT.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
                array(
                    'id' => "{$prefix}auto_seat_capacity",
                    'name' => __('Seating Capacity', 'dream-real-estate'),
                    'desc' => __('It will add seating capacity Ex. 4.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}auto_engine_type",
                    'name' => __('Engine Type', 'dream-real-estate'),
                    'desc' => __('It will add Engine Type Ex. TDCI Diesel Engine.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				array(
                    'id' => "{$prefix}auto_engine_description",
                    'name' => __('Engine Description', 'dream-real-estate'),
                    'desc' => __('It will add Engine Description Ex. 1.5KW.', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}auto_no_of_cylinders",
                    'name' => __('No. of Cylinders', 'dream-real-estate'),
                    'desc' => __('It will add Engine Description Ex. 1,2,3', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}auto_mileage_city",
                    'name' => __('Mileage City', 'dream-real-estate'),
                    'desc' => __('It will add Mileage City Ex. 22.4kmpl', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}auto_mileage_highway",
                    'name' => __('Mileage Highway', 'dream-real-estate'),
                    'desc' => __('It will add Mileage Highway Ex. 22.4kmpl', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				array(
                    'id' => "{$prefix}auto_fuel_tank_capacity",
                    'name' => __('Fuel Tank Capacity', 'dream-real-estate'),
                    'desc' => __('It will add Fuel Tank Capacity Ex. 40 (Liters)', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
				array(
                    'id' => "{$prefix}video_link",
                    'name' => __('Youtube Video Link:-', 'dream-real-estate'),
                    'desc' => __('It will Youtube Video link', 'dream-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'featured_auto',
                ),
				
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'specification_divider', // Not used, but needed
                    'tab' => 'featured_auto',
                ),

//		add_fea_pro			
				
                array(
                    'id' => "{$prefix}air_conditioner",
                    'name' => __('Air Conditioner', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
                    'tab' => 'add_fea_pro',
                ),

                array(
                    'id' => "{$prefix}braking_system",
                    'name' => __('AntiLock Braking System', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
                    'tab' => 'add_fea_pro',
                ),

                array(
                    'id' => "{$prefix}power_steering",
                    'name' => __('Power Steering', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
                    'tab' => 'add_fea_pro',
                ),
				
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'accessories_divider', // Not used, but needed
                    'tab' => 'add_fea_pro',
                ),

                array(
                    'id' => "{$prefix}power_window",
                    'name' => __('Power Windows', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
                    'tab' => 'add_fea_pro',
                ),

                array(
                    'id' => "{$prefix}indoorFireplace",
                    'name' => __('Indoor Fireplace', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}CD_player",
                    'name' => __('CD Player', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),
				
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'accessories_divider', // Not used, but needed
                    'tab' => 'add_fea_pro',
                ),
				
                array(
                    'id' => "{$prefix}leather_seats",
                    'name' => __('Leather Seats', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}central_locking",
                    'name' => __('Central Locking', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}power_door_lock",
                    'name' => __('Power Door Locks', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),
				
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'accessories_divider', // Not used, but needed
                    'tab' => 'add_fea_pro',
                ),
				
                array(
                    'id' => "{$prefix}brake_assist",
                    'name' => __('Brake Assist', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}driver_airbag",
                    'name' => __('Driver Airbag', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}passenger_airbag",
                    'name' => __('Passenger Airbag', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'accessories_divider', // Not used, but needed
                    'tab' => 'add_fea_pro',
                ),
                array(
                    'id' => "{$prefix}crash_sensor",
                    'name' => __('Crash Sensor', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}engine_check_warning",
                    'name' => __('Engine Check Warning', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),

				
                array(
                    'id' => "{$prefix}automatic_headlamps",
                    'name' => __('Automatic Headlamps', 'dream-real-estate'),
                    'type' => 'checkbox',
                    'std' => "",
                    'columns' => 4,
					'tab' => 'add_fea_pro',
                ),
            )
        );

        // apply a filter before returning meta boxes

        $meta_boxes = apply_filters( 'auto_meta_boxes', $meta_boxes );
        return $meta_boxes;
    }
    /*
     * Auto custom ID search support for properties index page on admin side

     ============================================================================= */
    /**
     * Check if current page is properties index page on admin side

     * @return bool

     */

    public function is_properties_index_page() {

        global $pagenow;

        return ( is_admin() && $pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'auto' && isset($_GET['s']) );

    }

    /**

     * Joins post meta table with posts table for search purpose

     * @param $join

     * @return string

     */
    public function join_post_meta_table( $join ) {
        global $wpdb;
        if ( $this->is_properties_index_page() ) {
            $join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }
        return $join;
    }

    /**

     * Add auto custom id in search

     *

     * @param $where

     * @return mixed

     */

    public function add_auto_id_in_search( $where ) {

        global $wpdb;

        if ( $this->is_properties_index_page() ) {

            $where = preg_replace(

                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",

                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_key = 'DREAM_auto_id') AND (".$wpdb->postmeta.".meta_value LIKE $1)",

                $where );

        }
        return $where;
    }
    /**

     * Add group by properties support

     * @param $group_by

     * @return string

     */
    function group_by_properties( $group_by ) {
        global $wpdb;
        if ( $this->is_properties_index_page() ) {
            $group_by = "$wpdb->posts.ID";
        }
        return $group_by;
    }
}