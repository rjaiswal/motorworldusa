<?php
/**
 * Represents a Auto Compare.
 *
 * This class provides utility functions related to a real estate auto.
 *
 *
 * @since      1.0.0
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/includes
 */
 
// Auto Compare Js Function  
function carforyou_Compare_List(){ ?>
	<script>
/*********************** Search product one ***********************************/
(function($){
  var $project = $('#autoSearch_1');
  	$('#autoSearch_1').keyup(function(e){
	var getproductname = document.getElementById("autoSearch_1").value;
	  $.ajax({ 
        data: {
			  action:'MyAjaxFunctionCompare', 
			  'productName':getproductname
			  },
        type: 'post',
        url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
         success: function(data)
		  {
			 var projects = data;
			  $project.autocomplete({
					minLength: 0,
					source: projects,
					focus: function( event, ui ) {						
					  $project.val( ui.item.label );
					  $("#p1").val( ui.item.id );
					  return false;
					}
				  });
				  
				  $project.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					
					var $li = $('<li onclick="getProduct_one('+item.id+')">'),
						$img = $('<img style="width:25px">');
				
					$img.attr({
					  src:item.icon,
					});
					$li.attr('data-value', item.label);
					$li.append('<a href="#">');
					$li.find('a').append(item.label).append($img);    
					return $li.appendTo(ul);
				};
		  }	
	          
	   }); 	   	
	});
})(jQuery);
/***********************End************************************************/
/*********************** Search product two ***********************************/
(function($){
  var $project = $('#autoSearch_2');
  	$('#autoSearch_2').keyup(function(e){
	var getproductname = document.getElementById("autoSearch_2").value;
	
	  $.ajax({ 
        data: {
			  action:'MyAjaxFunctionCompare', 
			  'productName':getproductname
			  },
         type: 'post',
        url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
         success: function(data)
		  {
			 var projects = data;
			  $project.autocomplete({
					minLength: 0,
					source: projects,
					focus: function( event, ui ) {
						
					  $project.val( ui.item.label );
					   $("#p2").val( ui.item.id );
					  return false;
					}
				  });
				  
				  $project.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					
					var $li = $('<li onclick="getProduct_two('+item.id+')">'),
						$img = $('<img style="width:25px">');
				
					$img.attr({
					  src:item.icon,
					});
					$li.attr('data-value', item.label);
					$li.append('<a href="#">');
					$li.find('a').append($img).append(item.label);    
					return $li.appendTo(ul);
				};
		  }	
	          
	   }); 	   	
	});
})(jQuery);
/***********************End************************************************/
/*********************** Search product three ***********************************/
(function($){
  var $project = $('#autoSearch_3');
  	$('#autoSearch_3').keyup(function(e){
	var getproductname = document.getElementById("autoSearch_3").value;
	  $.ajax({ 
        data: {
			  action:'MyAjaxFunctionCompare', 
			  'productName':getproductname
			  },
         type: 'post',
        url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
         success: function(data)
		  {
			 var projects = data;
			  $project.autocomplete({
					minLength: 0,
					source: projects,
					focus: function( event, ui ) {
						
					  $project.val( ui.item.label );
					   $("#p3").val( ui.item.id );
					  return false;
					}
				  });
				  
				  $project.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					
					var $li = $('<li onclick="getProduct_three('+item.id+')">'),
						$img = $('<img style="width:25px">');
				
					$img.attr({
					  src:item.icon,
					});
					$li.attr('data-value', item.label);
					$li.append('<a href="#">');
					$li.find('a').append($img).append(item.label);    
					return $li.appendTo(ul);
				};
		  }	
	          
	   }); 	   	
	});
})(jQuery);
/******************* Show product hide and show ***************************/
$(document).ready(function(){
	$("#autoSearch_1").on('keydown', function(e) {  
		if(e.keyCode == 13)
		{
			var productid = document.getElementById("p1").value;		
			getProduct_one(productid);
		}
	});
	$("#autoSearch_2").on('keydown', function(e) {  
		if(e.keyCode == 13)
		{
			var productid = document.getElementById("p2").value;
			getProduct_two(productid);
		}
	});
	$("#autoSearch_3").on('keydown', function(e) {  
		if(e.keyCode == 13)
		{
			var productid = document.getElementById("p3").value;
			getProduct_three(productid);
		}
	});
	
	$('#replace1').click(function(e){
				document.getElementById("project_search_1").style.display='block';
				document.getElementById("autoSearch_1").style.display='block';
				document.getElementById("autoSearch_2").style.display='none';
				document.getElementById("autoSearch_3").style.display='none';
				document.getElementById("autoSearch_1").value='';
				document.getElementById("autoSearch_2").value='';
				document.getElementById("autoSearch_3").value='';
	});
	$('#replace2').click(function(e){
				document.getElementById("project_search_2").style.display='block';
				document.getElementById("autoSearch_2").style.display='block';
				document.getElementById("autoSearch_3").style.display='none';
				document.getElementById("autoSearch_1").style.display='none';
				document.getElementById("autoSearch_1").value='';
				document.getElementById("autoSearch_2").value='';
				document.getElementById("autoSearch_3").value='';
	});
	$('#replace3').click(function(e){
				document.getElementById("project_search_3").style.display='block';
				document.getElementById("autoSearch_3").style.display='block';
				document.getElementById("autoSearch_1").style.display='none';
				document.getElementById("autoSearch_2").style.display='none';
				document.getElementById("autoSearch_1").value='';
				document.getElementById("autoSearch_2").value='';
				document.getElementById("autoSearch_3").value='';
	});
	$('#resetPhone1').click(function(e){
	  var changeProductImage = document.getElementById("pro_img_1");
				changeProductImage.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 	
				document.getElementById("autoSearch_1").style.display='none';
				document.getElementById("p1").value='';
					$('#auto_title_1').html('&nbsp;');
					$('#auto_price_1').html('&nbsp;');
					$('#auto_brand_1').html('&nbsp;');
					$("#phone1").html('&nbsp;');
					$('#auto_model_1').html('&nbsp;');
					$('#auto_model_year_1').html('&nbsp;');
					$('#auto_location_1').html('&nbsp;');
					$('#auto_condition_1').html('&nbsp;');
					$('#auto_kms_done_1').html('&nbsp;');
					$('#auto_body_color_1').html('&nbsp;');
					$('#seat_capacity_1').html('&nbsp;');
					$('#auto_transmission_1').html('&nbsp;');
					$('#auto_FuelType_1').html('&nbsp;');
					$('#auto_EngineType_1').html('&nbsp;');
					$('#auto_EngineDesc_1').html('&nbsp;');
					$('#auto_NoCylinder_1').html('&nbsp;');
					$('#auto_MileageCity_1').html('&nbsp;');
					$('#auto_MileageHighway_1').html('&nbsp;');
					$('#auto_FuelTank_1').html('&nbsp;');
					$('#auto_NoOwner_1').html('&nbsp;');
					
					$('#auto_air_condi_1').html('&nbsp;');
					$('#braking_system_1').html('&nbsp;');
					$('#power_steering_1').html('&nbsp;');
					$('#power_window_1').html('&nbsp;');
					$('#CD_player_1').html('&nbsp;');
					$('#leather_seats_1').html('&nbsp;');
					$('#central_locking_1').html('&nbsp;');
					$('#door_lock_1').html('&nbsp;');
					$('.auto_link_1').attr("href","#");


							   //set related product
				var compareProId='';
				var prd1=document.getElementById("p1").value;
				var prd2=document.getElementById("p2").value;
				var prd3=document.getElementById("p3").value;
				if(prd2>0)
					compareProId=prd2;
				if(prd3>0)
					compareProId=prd3;	
				if(prd2 >0 && prd3 >0)
					compareProId=prd2+","+prd3;	
				if(prd1 >0 && prd2 >0 && prd3 >0)
					compareProId=prd1+","+prd2+","+prd3;
				//call function for show related product	
				showRelatedProduct(1,compareProId);	
	});
	$('#resetPhone2').click(function(e){
	  var changeProductImage = document.getElementById("pro_img_2");
			changeProductImage.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 
			document.getElementById("autoSearch_2").style.display='none';	
			document.getElementById("p2").value='';

					$('#auto_title_2').html('&nbsp;');
					$('#auto_price_2').html('&nbsp;');
					$('#auto_brand_2').html('&nbsp;');
					$("#phone2").html('&nbsp;');
					$('#auto_model_2').html('&nbsp;');
					$('#auto_model_year_2').html('&nbsp;');
					$('#auto_location_2').html('&nbsp;');
					$('#auto_condition_2').html('&nbsp;');
					$('#auto_kms_done_2').html('&nbsp;');
					$('#auto_body_color_2').html('&nbsp;');
					$('#seat_capacity_2').html('&nbsp;');
					$('#auto_transmission_2').html('&nbsp;');
					$('#auto_FuelType_2').html('&nbsp;');
					$('#auto_EngineType_2').html('&nbsp;');
					$('#auto_EngineDesc_2').html('&nbsp;');
					$('#auto_NoCylinder_2').html('&nbsp;');
					$('#auto_MileageCity_2').html('&nbsp;');
					$('#auto_MileageHighway_2').html('&nbsp;');
					$('#auto_FuelTank_2').html('&nbsp;');
					$('#auto_NoOwner_2').html('&nbsp;');
					
					$('#auto_air_condi_2').html('&nbsp;');
					$('#braking_system_2').html('&nbsp;');
					$('#power_steering_2').html('&nbsp;');
					$('#power_window_2').html('&nbsp;');
					$('#CD_player_2').html('&nbsp;');
					$('#leather_seats_2').html('&nbsp;');
					$('#central_locking_2').html('&nbsp;');
					$('#door_lock_2').html('&nbsp;');
					$('.auto_link_2').attr("href","#");
					   
				//set related product
				var compareProId='';
				var prd1=document.getElementById("p1").value;
				var prd2=document.getElementById("p2").value;
				var prd3=document.getElementById("p3").value;	   
				if(prd1>0)
						compareProId=prd1;
					if(prd3>0)
						compareProId=prd3;	
					if(prd1 >0 && prd3 >0)
						compareProId=prd1+","+prd3;	
					if(prd1 >0 && prd2 >0 && prd3 >0)
						compareProId=prd1+","+prd2+","+prd3;
					//call function for show related product	
					showRelatedProduct(2,compareProId);
	});
	$('#resetPhone3').click(function(e){
	  var changeProductImage = document.getElementById("pro_img_3");
			changeProductImage.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 	
			document.getElementById("autoSearch_3").style.display='none';
			document.getElementById("p3").value='';


			$('#auto_title_3').html('&nbsp;');
			$('#auto_price_3').html('&nbsp;');
			$('#auto_brand_3').html('&nbsp;');
			$("#phone3").html('&nbsp;');
			$('#auto_model_3').html('&nbsp;');
			$('#auto_model_year_3').html('&nbsp;');
			$('#auto_location_3').html('&nbsp;');
			$('#auto_condition_3').html('&nbsp;');
			$('#auto_kms_done_3').html('&nbsp;');
			$('#auto_body_color_3').html('&nbsp;');
			$('#seat_capacity_3').html('&nbsp;');
			$('#auto_transmission_3').html('&nbsp;');
			$('#auto_FuelType_3').html('&nbsp;');
			$('#auto_EngineType_3').html('&nbsp;');
			$('#auto_EngineDesc_3').html('&nbsp;');
			$('#auto_NoCylinder_3').html('&nbsp;');
			$('#auto_MileageCity_3').html('&nbsp;');
			$('#auto_MileageHighway_3').html('&nbsp;');
			$('#auto_FuelTank_3').html('&nbsp;');
			$('#auto_NoOwner_3').html('&nbsp;');
			
			$('#auto_air_condi_3').html('&nbsp;');
			$('#braking_system_3').html('&nbsp;');
			$('#power_steering_3').html('&nbsp;');
			$('#power_window_3').html('&nbsp;');
			$('#CD_player_3').html('&nbsp;');
			$('#leather_seats_3').html('&nbsp;');
			$('#central_locking_3').html('&nbsp;');
			$('#door_lock_3').html('&nbsp;');
			$('.auto_link_3').attr("href","#");



					   
				//set related product
				var compareProId='';
				var prd1=document.getElementById("p1").value;
				var prd2=document.getElementById("p2").value;
				var prd3=document.getElementById("p3").value;	   
				if(prd1>0)
					compareProId=prd1;
				if(prd2>0)
					compareProId=prd2;	
				if(prd1 >0 && prd2 >0)
					compareProId=prd1+","+prd2;	
				if(prd1 >0 && prd2 >0 && prd3 >0)
					compareProId=prd1+","+prd2+","+prd3;
				//call function for show related product	
				showRelatedProduct(3,compareProId);
	});
});
//set compare from left side function use
function setCompareProduct(id)
{
		var prd1=document.getElementById("p1").value;
		var prd2=document.getElementById("p2").value;
		var prd3=document.getElementById("p3").value;
		
		if(prd1==id || prd2==id || prd3==id)
		   {
				
			alert("This product already exist in compare box!. Please select another product.");
		}
		else
		{
			if(prd1=='')
			{
				document.getElementById("p1").value=id;
				getProduct_one(id);
			}else if(prd2=='')
			{
				document.getElementById("p2").value=id;
				getProduct_two(id);
			}else if(prd3=='')
			{
				document.getElementById("p3").value=id;
				getProduct_three(id);
				
				}else{
					alert('Please remove any product from compare list.!');
					}	
		}
}
/******************* Find Product for comparison one ***************************/
function getProduct_one(id)
{	
	var productid = id;
	var compareProId='';
	var prd1=document.getElementById("p1").value;
	var prd2=document.getElementById("p2").value;
	var prd3=document.getElementById("p3").value;
	
	//check exits product
	if(prd2==id && prd2!='' || prd3==id && prd3!='')
	  {
		alert("This product already exist in compare box!. Please select another product.");
	  }
	  else
	  {
		if(prd1>0)
			compareProId=prd1;
		if(prd1 >0 && prd2 >0)
			compareProId=prd1+","+prd2;
		if(prd1 >0 && prd3 >0)
			compareProId=prd1+","+prd3;
		if(prd2 >0 && prd3 >0)
			compareProId=prd2+","+prd3;	
		if(prd1 >0 && prd2 >0 && prd3 >0)
			compareProId=prd1+","+prd2+","+prd3;
			$.ajax({ 
			data: {
				  action:'MyAjaxFunctionProduct_1', 
				  'productID':productid,'compareId':compareProId
				  },
			 type: 'post',
			 //'dataType': 'json', 
			 url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
			  success: function(data)
			  {
				   var productData = data.split('@');
				   //alert(productData);
				   var changeProductImage = document.getElementById("pro_img_1");
				   changeProductImage.src = productData[1];
				   $('#product_scroe_data').html(productData[2]);  
				   $( "#ProductDiv1" ).remove(); 
				   var phoneData = productData[0].split('#');
						$('#auto_title_1').html(phoneData[0]+ " and ");
						$('#auto_price_1').html(phoneData[1]);
						$('#auto_brand_1').html(phoneData[2]);
						$("#phone1").text(phoneData[0]);
						$('#auto_model_1').html(phoneData[4]);
						$('#auto_model_year_1').html(phoneData[5]);
						$('#auto_location_1').html(phoneData[6]);
						$('#auto_condition_1').html(phoneData[7]);
						$('#auto_kms_done_1').html(phoneData[8]);
						$('#auto_body_color_1').html(phoneData[9]);
						$('#seat_capacity_1').html(phoneData[10]);
						$('#auto_transmission_1').html(phoneData[11]);
						$('#auto_FuelType_1').html(phoneData[12]);
						$('#auto_EngineType_1').html(phoneData[13]);
						$('#auto_EngineDesc_1').html(phoneData[14]);
						$('#auto_NoCylinder_1').html(phoneData[15]);
						$('#auto_MileageCity_1').html(phoneData[16]);
						$('#auto_MileageHighway_1').html(phoneData[17]);
						$('#auto_FuelTank_1').html(phoneData[18]);
						$('#auto_NoOwner_1').html(phoneData[19]);
						
						$('#auto_air_condi_1').html(phoneData[20]);
						$('#braking_system_1').html(phoneData[21]);
						$('#power_steering_1').html(phoneData[22]);
						$('#power_window_1').html(phoneData[23]);
						$('#CD_player_1').html(phoneData[24]);
						$('#leather_seats_1').html(phoneData[25]);
						$('#central_locking_1').html(phoneData[26]);
						$('#door_lock_1').html(phoneData[27]);
						$('.auto_link_1').attr("href",phoneData[28]);
						
				   document.getElementById("autoSearch_1").style.display='none';
				 }
			});
	  }
	 
	  
}
/******************* Find Product for comparison two***************************/
function getProduct_two(id)
{	
	var productid = id;
	var compareProId='';
	var prd1=document.getElementById("p1").value;
	var prd2=document.getElementById("p2").value;
	var prd3=document.getElementById("p3").value;
	
	//check exits product
	if(prd1==id && prd1!='' || prd3==id && prd3!='')
	  {
		alert("This product already exist in compare box!. Please select another product.");
	  }
	  else
	  {
			if(prd2>0)
				compareProId=prd2;
			if(prd1 >0 && prd2 >0)
				compareProId=prd1+","+prd2;
			if(prd1 >0 && prd3 >0)
				compareProId=prd1+","+prd3;
			if(prd2 >0 && prd3 >0)
				compareProId=prd2+","+prd3;	
			if(prd1 >0 && prd2 >0 && prd3 >0)
				compareProId=prd1+","+prd2+","+prd3;
				
				$.ajax({ 
				data: {
					  action:'MyAjaxFunctionProduct_2', 
					  'productID':productid,'compareId':compareProId
					  },
				 type: 'post',
				 //'dataType': 'json', 
				 url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
				  success: function(data)
				  {
					  var productData = data.split('@');
					  var changeProductImage = document.getElementById("pro_img_2");
					  changeProductImage.src = productData[1]; 
					    $('#product_scroe_data').html(productData[2]);  
				   $( "#ProductDiv1" ).remove(); 
				   var phoneData = productData[0].split('#');
				   
						$('#auto_title_2').html(phoneData[0]);
						$('#auto_price_2').html(phoneData[1]);
						$('#auto_brand_2').html(phoneData[2]);
						$("#phone2").text(phoneData[0]);
						$('#auto_model_2').html(phoneData[4]);
						$('#auto_model_year_2').html(phoneData[5]);
						$('#auto_location_2').html(phoneData[6]);
						$('#auto_condition_2').html(phoneData[7]);
						$('#auto_kms_done_2').html(phoneData[8]);
						$('#auto_body_color_2').html(phoneData[9]);
						$('#seat_capacity_2').html(phoneData[10]);
						$('#auto_transmission_2').html(phoneData[11]);
						$('#auto_FuelType_2').html(phoneData[12]);
						$('#auto_EngineType_2').html(phoneData[13]);
						$('#auto_EngineDesc_2').html(phoneData[14]);
						$('#auto_NoCylinder_2').html(phoneData[15]);
						$('#auto_MileageCity_2').html(phoneData[16]);
						$('#auto_MileageHighway_2').html(phoneData[17]);
						$('#auto_FuelTank_2').html(phoneData[18]);
						$('#auto_NoOwner_2').html(phoneData[19]);
						
						$('#auto_air_condi_2').html(phoneData[20]);
						$('#braking_system_2').html(phoneData[21]);
						$('#power_steering_2').html(phoneData[22]);
						$('#power_window_2').html(phoneData[23]);
						$('#CD_player_2').html(phoneData[24]);
						$('#leather_seats_2').html(phoneData[25]);
						$('#central_locking_2').html(phoneData[26]);
						$('#door_lock_2').html(phoneData[27]);
						$('.auto_link_2').attr("href",phoneData[28]);
						
						
					  document.getElementById("autoSearch_2").style.display='none';
				  }
				});
	  }
}
/******************* Find Product for comparison ***************************/
function getProduct_three(id)
{	
	var productid = id;
	var compareProId='';
	var prd1=document.getElementById("p1").value;
	var prd2=document.getElementById("p2").value;
	var prd3=document.getElementById("p3").value;
	
	//check exits product
	if(prd1==id && prd1!='' || prd2==id && prd2!='')
	  {
		alert("This product already exist in compare box!. Please select another product.");
	  }
	  else
	  {
		
			if(prd3>0)
				compareProId=prd3;
			if(prd1 >0 && prd2 >0)
				compareProId=prd1+","+prd2;
			if(prd1 >0 && prd3 >0)
				compareProId=prd1+","+prd3;
			if(prd2 >0 && prd3 >0)
				compareProId=prd2+","+prd3;	
			if(prd1 >0 && prd2 >0 && prd3 >0)
				compareProId=prd1+","+prd2+","+prd3;
				
		
				$.ajax({ 
				data: {
					  action:'MyAjaxFunctionProduct_3', 
					  'productID':productid,'compareId':compareProId
					  },
				 type: 'post',
				 //'dataType': 'json', 
				 url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
				  success: function(data)
				  {
					 var productData = data.split('@');
					  var changeProductImage = document.getElementById("pro_img_3");
					  changeProductImage.src = productData[1];
					 
					  $('#product_scroe_data').html(productData[2]); 
					  $("#ProductDiv3" ).remove();
					   var phoneData = productData[0].split('#');
					   
						$('#auto_title_3').html("and. "+phoneData[0]);
						$('#auto_price_3').html(phoneData[1]);
						$('#auto_brand_3').html(phoneData[2]);
						$("#phone3").text(phoneData[0]);
						$('#auto_model_3').html(phoneData[4]);
						$('#auto_model_year_3').html(phoneData[5]);
						$('#auto_location_3').html(phoneData[6]);
						$('#auto_condition_3').html(phoneData[7]);
						$('#auto_kms_done_3').html(phoneData[8]);
						$('#auto_body_color_3').html(phoneData[9]);
						$('#seat_capacity_3').html(phoneData[10]);
						$('#auto_transmission_3').html(phoneData[11]);
						$('#auto_FuelType_3').html(phoneData[12]);
						$('#auto_EngineType_3').html(phoneData[13]);
						$('#auto_EngineDesc_3').html(phoneData[14]);
						$('#auto_NoCylinder_3').html(phoneData[15]);
						$('#auto_MileageCity_3').html(phoneData[16]);
						$('#auto_MileageHighway_3').html(phoneData[17]);
						$('#auto_FuelTank_3').html(phoneData[18]);
						$('#auto_NoOwner_3').html(phoneData[19]);
						
						$('#auto_air_condi_3').html(phoneData[20]);
						$('#braking_system_3').html(phoneData[21]);
						$('#power_steering_3').html(phoneData[22]);
						$('#power_window_3').html(phoneData[23]);
						$('#CD_player_3').html(phoneData[24]);
						$('#leather_seats_3').html(phoneData[25]);
						$('#central_locking_3').html(phoneData[26]);
						$('#door_lock_3').html(phoneData[27]);
						$('.auto_link_3').attr("href",phoneData[28]);
						
					  document.getElementById("autoSearch_3").style.display='none';
					  $( "#li3" ).addClass("active");
				  }
				});
	  }
}
/*********************Reset all data *********************************/
function resectData()
{
	  var changeProductImage = document.getElementById("pro_img_1");
	  changeProductImage.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 
	  var changeProductImage1 = document.getElementById("pro_img_2");
	  changeProductImage1.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 
	  var changeProductImage2 = document.getElementById("pro_img_3");
	  changeProductImage2.src ='<?php echo esc_url( get_template_directory_uri() )?>/assets/images/auto-img.png'; 
	  $("#li3" ).removeClass("active");
	  document.getElementById("p1").value='';
	  document.getElementById("p2").value='';
	  document.getElementById("p3").value='';
	  //reset phone data one
			$('#auto_title_1').html('&nbsp;');
			$('#auto_price_1').html('&nbsp;');
			$('#auto_brand_1').html('&nbsp;');
			$("#phone1").html('&nbsp;');
			$('#auto_model_1').html('&nbsp;');
			$('#auto_model_year_1').html('&nbsp;');
			$('#auto_location_1').html('&nbsp;');
			$('#auto_condition_1').html('&nbsp;');
			$('#auto_kms_done_1').html('&nbsp;');
			$('#auto_body_color_1').html('&nbsp;');
			$('#seat_capacity_1').html('&nbsp;');
			$('#auto_transmission_1').html('&nbsp;');
			$('#auto_FuelType_1').html('&nbsp;');
			$('#auto_EngineType_1').html('&nbsp;');
			$('#auto_EngineDesc_1').html('&nbsp;');
			$('#auto_NoCylinder_1').html('&nbsp;');
			$('#auto_MileageCity_1').html('&nbsp;');
			$('#auto_MileageHighway_1').html('&nbsp;');
			$('#auto_FuelTank_1').html('&nbsp;');
			$('#auto_NoOwner_1').html('&nbsp;');
			
			$('#auto_air_condi_1').html('&nbsp;');
			$('#braking_system_1').html('&nbsp;');
			$('#power_steering_1').html('&nbsp;');
			$('#power_window_1').html('&nbsp;');
			$('#CD_player_1').html('&nbsp;');
			$('#leather_seats_1').html('&nbsp;');
			$('#central_locking_1').html('&nbsp;');
			$('#door_lock_1').html('&nbsp;');
			$('.auto_link_1').attr("href","#");

   //Reset Auto Data Two
			$('#auto_title_2').html('&nbsp;');
			$('#auto_price_2').html('&nbsp;');
			$('#auto_brand_2').html('&nbsp;');
			$("#phone2").html('&nbsp;');
			$('#auto_model_2').html('&nbsp;');
			$('#auto_model_year_2').html('&nbsp;');
			$('#auto_location_2').html('&nbsp;');
			$('#auto_condition_2').html('&nbsp;');
			$('#auto_kms_done_2').html('&nbsp;');
			$('#auto_body_color_2').html('&nbsp;');
			$('#seat_capacity_2').html('&nbsp;');
			$('#auto_transmission_2').html('&nbsp;');
			$('#auto_FuelType_2').html('&nbsp;');
			$('#auto_EngineType_2').html('&nbsp;');
			$('#auto_EngineDesc_2').html('&nbsp;');
			$('#auto_NoCylinder_2').html('&nbsp;');
			$('#auto_MileageCity_2').html('&nbsp;');
			$('#auto_MileageHighway_2').html('&nbsp;');
			$('#auto_FuelTank_2').html('&nbsp;');
			$('#auto_NoOwner_2').html('&nbsp;');
			
			$('#auto_air_condi_2').html('&nbsp;');
			$('#braking_system_2').html('&nbsp;');
			$('#power_steering_2').html('&nbsp;');
			$('#power_window_2').html('&nbsp;');
			$('#CD_player_2').html('&nbsp;');
			$('#leather_seats_2').html('&nbsp;');
			$('#central_locking_2').html('&nbsp;');
			$('#door_lock_2').html('&nbsp;');
			$('.auto_link_2').attr("href","#");

	//reset phone data three
	 
			$('#auto_title_3').html('&nbsp;');
			$('#auto_price_3').html('&nbsp;');
			$('#auto_brand_3').html('&nbsp;');
			$("#phone3").html('&nbsp;');
			$('#auto_model_3').html('&nbsp;');
			$('#auto_model_year_3').html('&nbsp;');
			$('#auto_location_3').html('&nbsp;');
			$('#auto_condition_3').html('&nbsp;');
			$('#auto_kms_done_3').html('&nbsp;');
			$('#auto_body_color_3').html('&nbsp;');
			$('#seat_capacity_3').html('&nbsp;');
			$('#auto_transmission_3').html('&nbsp;');
			$('#auto_FuelType_3').html('&nbsp;');
			$('#auto_EngineType_3').html('&nbsp;');
			$('#auto_EngineDesc_3').html('&nbsp;');
			$('#auto_NoCylinder_3').html('&nbsp;');
			$('#auto_MileageCity_3').html('&nbsp;');
			$('#auto_MileageHighway_3').html('&nbsp;');
			$('#auto_FuelTank_3').html('&nbsp;');
			$('#auto_NoOwner_3').html('&nbsp;');
			
			$('#auto_air_condi_3').html('&nbsp;');
			$('#braking_system_3').html('&nbsp;');
			$('#power_steering_3').html('&nbsp;');
			$('#power_window_3').html('&nbsp;');
			$('#CD_player_3').html('&nbsp;');
			$('#leather_seats_3').html('&nbsp;');
			$('#central_locking_3').html('&nbsp;');
			$('#door_lock_3').html('&nbsp;');
			$('.auto_link_3').attr("href","#");
	
					   
				//call function for show related product
				$compareProId='';	
				showRelatedProduct('',$compareProId);		   
}

//show all related mobda scroe product
function showRelatedProduct(session_val,id){
	var compareProId = id;		
		$.ajax({ 
        data: {
			  action:'MyAjaxFunctionProduct_related', 
			  'compareId':compareProId,'sessVal':session_val
			  },
         type: 'post',
		 //'dataType': 'json', 
         url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
		  success: function(data)
		  {	
		   	//set all related product		  
			$('#product_scroe_data').html(data);  
		  }
		});
}
</script>
<?php }
add_shortcode('Auto_Compare', 'carforyou_Compare_List');

function carforyou_Compare_getData(){?>
<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_REQUEST['p1']) and $_REQUEST['p1']!='')
{?>
<script type="text/javascript">
document.getElementById("p1").value=<?php echo $_REQUEST['p1']; ?>;
getProduct_one('<?php echo $_REQUEST['p1']; ?>')
</script>	
<?php }
if(isset($_REQUEST['p2']) and $_REQUEST['p2']!='')
{ ?>
<script type="text/javascript">
document.getElementById("p2").value=<?php echo $_REQUEST['p2']; ?>;
getProduct_two('<?php echo $_REQUEST['p2']; ?>')
</script>	
<?php }
if(isset($_REQUEST['p3']) and $_REQUEST['p3']!='')
{ ?>
<script type="text/javascript">
document.getElementById("p3").value=<?php echo $_REQUEST['p3']; ?>;
getProduct_three('<?php echo $_REQUEST['p3']; ?>')
</script>	
<?php }	
}
add_shortcode('carforyou_Compare_getData', 'carforyou_Compare_getData');

// Auto Compare Popup Ajax
function carforyou_Compare_PopupAjax(){?>
<script> 
function productShort()
{
	document.getElementById('shortProduct').submit();
}
document.getElementById("p1").value='';
document.getElementById("p2").value='';
document.getElementById("p3").value='';
   function productCompare(id){
	  	document.getElementById("add_model_compare").style.display='block';
  	var productid = id;
	$.ajax({ 
      data: {
		  action:'MyAjaxFunctionProductShop', 
		  'productID':productid
		  },
        type: 'post',
	 //'dataType': 'json', 
     url: "<?php echo esc_url(home_url()); ?>/wp-admin/admin-ajax.php",
	  success: function(data)
	  {
	 var productData = data.split('@');
	 var product_text_one=document.getElementById('p1').value;
	 var product_text_two=document.getElementById('p2').value;
	 var product_text_three=document.getElementById('p3').value;
	 if(product_text_one=='')
		 {
		  var changeProductImage = document.getElementById("pro_img_1");
		  changeProductImage.src = productData[1];
		  document.getElementById('p1').value=productData[0];
		  document.getElementById("countProduct").textContent='1';
		 }
		 
		 else if(product_text_two=='' && product_text_one!=productData[0])
		 {
			  var changeProductImage = document.getElementById("pro_img_2");
	      	changeProductImage.src = productData[1]; 
		  document.getElementById('p2').value=productData[0];
	    document.getElementById("countProduct").textContent='2';
			}else if(product_text_three=='' && product_text_one!=productData[0] && product_text_two!=productData[0])
			{
			  var changeProductImage = document.getElementById("pro_img_3");
	 			 changeProductImage.src = productData[1];
				   document.getElementById('p3').value=productData[0];
				     document.getElementById("countProduct").textContent='3';
					 document.getElementById("productCompareForm").submit();
				}else
				{
					alert("This auto already exist in compare box!. Please select another auto.");
					}	
	  }
		});
   }
   function closePopUp(){
   document.getElementById("add_model_compare").style.display='none';
   }
   function formSubmit(){
	   document.getElementById("productCompareForm").submit();
   }
</script>
<?php 
}

//******************BeEvent Countdown Timer Shortcode ***********************//
function carforyou_countdown_timer(){?>
<script type="text/javascript">
<?php $countdown_timer = carforyou_get_option('countdown_timer'); ?>
var endDate = "<?php echo $countdown_timer;?>";
$('.countdown.styled').countdown({
date: endDate,
render: function(data) {
$(this.el).html("<div class='countdown-amount'>" + this.leadingZeros(data.days, 2) + " <span class='countdown-period'>Days</span></div><div class='countdown-amount'>" + this.leadingZeros(data.hours, 2) + " <span class='countdown-period'>Hours</span></div><div class='countdown-amount'>" + this.leadingZeros(data.min, 2) + " <span class='countdown-period'>Minutes</span></div><div class='countdown-amount'>" + this.leadingZeros(data.sec, 2) + " <span class='countdown-period'>Seconds</span></div>");
  }
});
</script>
<?php 
}
add_shortcode('Countdown_Timer', 'carforyou_countdown_timer' );

// Auto Brand Filter
function carforyou_Brand_Filter(){?>
	<script type="text/javascript">
$(document).ready(function(){
$('#autobrand').on('change',function(){
	var autobrandID = $(this).val();
	if(autobrandID){
		$.ajax({
			type:'POST',
			url:'<?php echo admin_url('admin-ajax.php'); ?>',
			data: {
			   action:'SelectModel', 
			  'autobrandName':autobrandID
	  },
			success:function(html){
				$('#automodel').html(html);
			}
		}); 
	}
});
});
</script>
<?php 
}

add_shortcode('FilterForm1', 'carforyouFilterForm');
add_shortcode('FilterForm2', 'carforyou_FilterForm2');
