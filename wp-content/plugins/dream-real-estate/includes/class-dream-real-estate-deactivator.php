<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://themeforest.net/user/DreamThemes
 * @since      1.0.0
 *
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/includes
 * @author     M Saqib Sarwar <saqib@dreamthemes.com>
 */
class Dream_Real_Estate_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
