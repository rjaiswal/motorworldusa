<?php
/**
 * Represents a Auto Function.
 *
 * This class provides utility functions related to a real estate auto.
 *
 *
 * @since      1.0.0
 * @package    Dream_Real_Estate
 * @subpackage Dream_Real_Estate/includes
 */
// FeaturedCar Function  
add_shortcode('AutoFeaturedCars', 'carforyou_FeaturedCar');
// UseNewCar Function
add_shortcode('AutoUseNewCarFilter', 'carforyou_UseNewCar');
// Team Member Function
add_shortcode('Team_Member_List', 'carforyou_team');
//Testimonial Style 1 Function 
add_shortcode('Testimonials_List_1', 'carforyou_testimonial');
// Testimonial Style 2 Function
add_shortcode('Testimonials_List_2', 'carforyou_testimonial_2');
//Latest Blog List 
add_shortcode('Latest_Blog_List', 'carforyou_Latestblog');
// Trending Car List 
add_shortcode('TrendCar_List', 'carforyou_trendcar');
// Home Slider 
add_shortcode('Slider', 'carforyou_Slider');
// Theme Font
function carforyou_font_family(){
	$font_style = carforyou_get_option('font_style');	
	if($font_style=='Montserrat'):
		echo "<style>body{ font-family: 'Montserrat', sans-serif !important;}</style>";	 
	elseif($font_style=='Roboto'):
		echo "<style> body{ font-family: 'Roboto', sans-serif !important; }</style>";	 
	elseif($font_style=='MartelSans'):
		echo "<style> body{ font-family: 'Martel Sans', sans-serif !important; }</style>";	 
	elseif($font_style=='OpenSans'):
		echo "<style> body{ font-family: 'Open Sans', sans-serif !important; }</style>";	 
	else:	
endif;
}

function carforyou_pc(){
$p_c = carforyou_get_option('primary_color');
$h_c = carforyou_get_option('hover_color');
if(!empty($p_c)):?>
<style>
.btn, 
.nav-tabs > li.active > a, 
.nav-tabs > li.active > a:focus, 
.nav-tabs > li.active > a:hover, 
.recent-tab .nav.nav-tabs li.active a, 
.fun-facts-m .vc_column-inner, .featured-icon, 
.owl-pagination .owl-page.active,
#testimonial-slider .owl-pagination .owl-page.active, 
.social-follow.footer-social a:hover, 
.back-top a, 
.team_more_info ul li a:hover, 
.tag_list ul li a:hover, 
.pagination ul li.current, 
.pagination ul li:hover,
.btn.outline:hover, 
.btn.outline:focus, 
.share_article ul li:hover, 
.nav-tabs > li a:hover, 
.nav-tabs > li a:focus, 
.label-icon, 
.navbar-default .navbar-toggle .icon-bar, 
.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover, 
.label_icon, 
.navbar-nav > li > .dropdown-menu, 
.add_compare .checkbox, 
.search_other, 
.vs, 
.td_divider, 
.search_other_inventory, 
#other_info, 
.main_bg, 
.slider .slider-handle, .slider .slider-selection, .listing_detail_wrap .nav-tabs > li.active a, .listing_detail_wrap .nav-tabs > li:hover a, input[type="submit"], .owl-carousel .owl-dot.active {
  background: <?php echo $p_c;?> none repeat scroll 0 0;
  fill: <?php echo $p_c;?>;
}
a, .btn-link, .car-title-m h6 a:hover, .featured-car-content > h6 a:hover, .footer-top ul li a:hover, .get-intouch a:hover, .blog-content h5 a:hover, .blog-info-box li a:hover, .control-label span, .angle_arrow i, .contact_detail li a:hover, .team_more_info p a:hover, .error_text_m h2, .search_btn, .popular_post_title a:hover, .categories_list ul li a:hover, .categories_list ul li a:hover::after, .article_meta ul li a:hover, .articale_header h2 a:hover, .btn.outline, .share_article ul li, .contact-info a:hover, .social-follow a:hover, .radio input[type="radio"]:checked + label::before, .checkbox input[type="checkbox"]:checked + label::before, .product-listing-content h5 a:hover, .pricing_info .price, .text-primary, .footer_widget ul li a:hover, .header_search button:hover, .header_widgets a:hover, .navbar-default .navbar-nav > li.active a, .navbar-default .navbar-nav > li:focus a, .navbar-default .navbar-nav > li:hover a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover, .my_vehicles_list ul.vehicle_listing li a:hover, .dealer_contact_info a:hover, .widget_heading i, .dealers_listing .dealer_info h5 a:hover, .main_features ul li p, .listing_detail_head .price_info p, .listing_other_info button:hover, .compare_info table td i, .compare_info table th i, #accessories i, .price, .inventory_info_list ul li i, .services_info h4 a:hover, .about_info .icon_box, .header_style2 .navbar-nav > li > .dropdown-menu a:hover, .header_style2 .navbar-default .navbar-nav li:hover .dropdown-menu li a:hover, .header_style2 .dropdown-menu > .active > a, .header_style2 .dropdown-menu > .active > a:focus, .header_style2 .dropdown-menu > .active > a:hover, .header_style2 .dropdown-menu > li > a:focus, .header_style2 .dropdown-menu > li > a:hover, a:hover, a:focus, .btn-link:hover, .sidebar_widget ul li a:hover, .sidebar_widget ul#recentcomments li a{
	
color:<?php echo $p_c;?>;
    fill:<?php echo $p_c;?>;	
	
	}


.btn:hover, .btn:focus, 
.search_other:hover, 
#other_info:hover {
	background-color: <?php echo $h_c;?>;
	fill: <?php echo $h_c;?>;
}

.nav-tabs > li.active > a, 
.nav-tabs > li.active > a:focus, 
.nav-tabs > li.active > a:hover, 
.social-follow.footer-social a:hover, 
.page-header, 
.tag_list ul li a:hover, 
.btn.outline, 
.share_article ul li, 
blockquote, 
.social-follow a:hover, 
.radio label:before,  
.navbar-default .navbar-toggle, 
.owl-buttons div, 
.about_info .icon_box {
	border-color: <?php echo $p_c;?>;
}

.recent-tab .nav.nav-tabs li.active::after {
	border-color: <?php echo $p_c;?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
}
.td_divider:after {
	border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0 ) rgba(0, 0, 0, 0 ) <?php echo $p_c;?> ;
}

.navbar-nav > li > .dropdown-menu li {
  border-bottom: 1px solid <?php echo $p_c;?>;
}

.payment-price.form-control {
  border: 1px solid <?php echo $p_c;?> !important;
  color: <?php echo $p_c;?> !important;
}

@media (max-width:767px) {
.navbar-default .navbar-nav .open .dropdown-menu > li > a:focus, .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover {
	color:<?php echo $p_c;?>;	
}
}
</style>
<?php 
endif;
}
// Header Image
function carforyou_header_image(){
	global $post;
	if(!empty($post->auto_header_image)):
		$images = rwmb_meta('auto_header_image', 'size=full');
		foreach ($images as $image):
		  echo esc_url($image['url']);
		endforeach;
	else:
	$carforyou_Inner_Img = carforyou_get_option('InnerPageImg');
		if (!empty($carforyou_Inner_Img['url'])):
			echo esc_url($carforyou_Inner_Img['url']);
		endif;
	endif;
}
// Min Price
function carforyou_min_price(){
    global $wpdb;
	$table_name = $wpdb->prefix . "postmeta"; 
    $query = $wpdb->get_var("SELECT min(cast(meta_value as unsigned)) FROM $table_name WHERE meta_key='DREAM_auto_price'");
	echo esc_html($query);		
}
// Max Price
function carforyou_max_price(){
    global $wpdb;
	$table_name = $wpdb->prefix . "postmeta"; 
    $query = $wpdb->get_var("SELECT max(cast(meta_value as unsigned)) FROM $table_name WHERE meta_key='DREAM_auto_price'");
	echo esc_html($query);
}