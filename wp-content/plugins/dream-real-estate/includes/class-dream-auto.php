<?php

/**

 * Represents a real estate auto.

 *

 * This class provides utility functions related to a real estate auto.

 *

 *

 * @since      1.0.0

 * @package    Dream_Real_Estate

 * @subpackage Dream_Real_Estate/includes

 * @author     M Saqib Sarwar <saqib@dreamthemes.com>

 */



class Dream_Auto {



    /**

     * @var int $auto_id contains auto post id.

     */

    private $auto_id;



    /**

     * @var array auto meta keys

     */

    private $meta_keys = array(

        'price'                 => 'DREAM_auto_price',

        'price_postfix'         => 'DREAM_auto_price_postfix',

        'custom_id'             => 'DREAM_auto_id',

        'area'                  => 'DREAM_auto_size',

        'area_postfix'          => 'DREAM_auto_size_postfix',

        'beds'                  => 'DREAM_auto_bedrooms',

        'baths'                 => 'DREAM_auto_bathrooms',

        'garages'               => 'DREAM_auto_garage',

        'additional_details'    => 'DREAM_additional_details',

        'address'               => 'DREAM_auto_address',

        'map_location'          => 'DREAM_auto_location',

        'attachments'           => 'DREAM_attachments',

        'agent_display_option'  => 'DREAM_agent_display_option',

        'agent_id'              => 'DREAM_agents',

        'slider_image'          => 'DREAM_slider_image',

        'payment_status'        => 'payment_status',

        'video_url'             => 'DREAM_tour_video_url',

        'video_image'           => 'DREAM_tour_video_image',

    );



    /**

     * @var array   $meta_data  contains custom fields data related to a auto

     */

    private $meta_data;





    /**

     * @param int $auto_id

     */

    public function __construct( $auto_id = null ) {



        if ( !$auto_id ) {

            $auto_id = get_the_ID();

        } else {

            $auto_id = intval( $auto_id );

        }



        if ( $auto_id > 0 ) {

            $this->auto_id = $auto_id;

            $this->meta_data = get_post_custom( $auto_id );

        }



    }



    /**

     * Return auto meta

     *

     * @param $meta_key

     * @return mixed

     */

    public function get_auto_meta( $meta_key ) {

        if ( isset( $this->meta_data[ $meta_key ] ) ) {

            return $this->meta_data[ $meta_key ][0];

        } else {

            return false;

        }

    }



    /**

     * Return auto post id

     * @return bool|mixed

     */

    public function get_post_ID(){

        return $this->auto_id;

    }



    /**

     * Return auto custom id

     * @return bool|mixed

     */

    public function get_custom_ID(){

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['custom_id'] );

    }



    /**

     * Return auto area

     * @return bool|mixed

     */

    public function get_area(){

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['area'] );

    }



    /**

     * Return auto area postfix

     * @return bool|mixed

     */

    public function get_area_postfix(){

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['area_postfix'] );

    }



    /**

     * Return auto beds

     * @return bool|mixed

     */

    public function get_beds(){

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['beds'] );

    }



    /**

     * Return auto baths

     * @return bool|mixed

     */

    public function get_baths(){

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['baths'] );

    }



    /**

     * Return auto garages

     * @return bool|mixed

     */

    public function get_garages() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['garages'] );

    }



    /**

     * Return auto additional details

     * @return bool|mixed

     */

    public function get_additional_details() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return maybe_unserialize ( $this->get_auto_meta( $this->meta_keys['additional_details'] ) );

    }



    /**

     * Return auto address

     * @return bool|mixed

     */

    public function get_address() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['address'] );

    }



    /**

     * Return auto location, A string containing comma separated values of latitude and longitude

     * @return bool|mixed

     */

    public function get_location() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['map_location'] );

    }



    /**

     * Return latitude value

     * @return bool|string

     */

    public function get_latitude() {

        $location = $this->get_location();

        if ( $location ) {

            $lat_lng = explode( ',', $location );

            if( is_array( $lat_lng ) && isset( $lat_lng[0] ) ) {

                return $lat_lng[0];

            }

        }

        return false;

    }



    /**

     * Return longitude value

     * @return bool|string

     */

    public function get_longitude() {

        $location = $this->get_location();

        if ( $location ) {

            $lat_lng = explode( ',', $location );

            if( is_array( $lat_lng ) && isset( $lat_lng[1] ) ) {

                return $lat_lng[1];

            }

        }

        return false;

    }



    /**

     * Return attachments array

     * @return bool|mixed

     */

    public function get_attachments() {

        if ( !$this->auto_id ) {

            return false;

        }

        if ( isset( $this->meta_data[ $this->meta_keys['attachments'] ] ) ) {

            return $this->meta_data[ $this->meta_keys['attachments'] ];

        }

        return false;

    }



    /**

     * Get agent display option

     * @return bool|mixed

     */

    public function get_agent_display_option() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['agent_display_option'] );

    }



    /**

     * Get agent id

     * @return bool|mixed

     */

    public function get_agent_id() {

        if ( ! $this->auto_id ) {

            return false;

        }

        return $this->get_auto_meta( $this->meta_keys['agent_id'] );

    }



    /**

     * Get slider image URL

     * @return bool|string

     */

    public function get_slider_image() {

        if ( ! $this->auto_id ) {

            return false;

        }



        $slider_image_id = $this->get_auto_meta( $this->meta_keys['slider_image'] );

        if ( $slider_image_id ) {

            return wp_get_attachment_url( $slider_image_id );

        }



        return false;

    }



    /**

     * Display auto price

     */

    public function price() {

        if ( ! $this->auto_id ) {

            return false;

        }

        echo $this->get_price();

    }



    /**

     * Returns auto price

     * @return string price

     */

    public function get_price() {

        if ( ! $this->auto_id ) {

            return null;

        }

        $price_amount = doubleval( $this->get_auto_meta( $this->meta_keys[ 'price' ] ) );

        $price_postfix = "<span>".$this->get_auto_meta( $this->meta_keys[ 'price_postfix' ] )."</span>";

        return $this->format_price( $price_amount, $price_postfix );

    }



    /**

     * Provide formatted price

     *

     * @param int|double|float $price_amount    price amount

     * @param string $price_postfix    price post fix

     * @return null|string  formatted price

     */

    public static function format_price ( $price_amount, $price_postfix = '' ) {



        // get related plugin options

        $dream_real_estate = Dream_Real_Estate::get_instance();



        if( $price_amount ) {



            $currency_sign      = $dream_real_estate->get_currency_sign();

            $number_of_decimals = $dream_real_estate->get_number_of_decimals();

            $decimal_separator  = $dream_real_estate->get_decimal_separator();

            $thousand_separator = $dream_real_estate->get_thousand_separator();

            $currency_position  = $dream_real_estate->get_currency_position();



            // format price

            $formatted_price = number_format( $price_amount, $number_of_decimals, $decimal_separator, $thousand_separator );



            // add currency and post fix

            if( $currency_position == 'after' ) {

                $formatted_price = $formatted_price . $currency_sign;

            } else {

                $formatted_price = $currency_sign . $formatted_price;

            }



            if ( !empty( $price_postfix ) ) {

                $formatted_price = $formatted_price . ' ' . $price_postfix;

            }



            return $formatted_price;



        } else {



            return $dream_real_estate->get_empty_price_text();



        }



    }



    /**

     * Returns auto price without postfix

     * @return string price

     */

    public function get_price_without_postfix() {

        if ( ! $this->auto_id ) {

            return null;

        }

        $price_amount = doubleval( $this->get_auto_meta( $this->meta_keys[ 'price' ] ) );

        return $this->format_price( $price_amount );

    }



    /**

     * Returns auto price postfix

     * @return string price postfix

     */

    public function get_price_postfix() {

        if ( ! $this->auto_id ) {

            return null;

        }

        $price_postfix = $this->get_auto_meta( $this->meta_keys[ 'price_postfix' ] );

        return $price_postfix;

    }



    /**

     * Returns payment status of auto

     * @return mixed|null

     */

    public function get_payment_status() {

        if ( ! $this->auto_id ) {

            return null;

        }

        $payment_status = $this->get_auto_meta( $this->meta_keys[ 'payment_status' ] );

        return $payment_status;

    }



    /**

     * Returns url of video if exists

     * @return mixed|null

     */

    public function get_video_url() {

        if ( !$this->auto_id ) {

            return null;

        }

        $video_url = $this->get_auto_meta( $this->meta_keys[ 'video_url' ] );

        return $video_url;

    }



    /**

     * Return video image id if exists

     * @return mixed|null

     */

    public function get_video_image() {

        if ( !$this->auto_id ) {

            return null;

        }

        $video_url = $this->get_auto_meta( $this->meta_keys[ 'video_image' ] );

        return $video_url;

    }



    /**

     * Return auto types

     * @return bool|null|string

     */

    public function get_types() {

        return $this->get_taxonomy_terms( 'auto-type' );

    }



    /**

     * Return auto status

     * @return bool|null|string

     */

    public function get_status() {

        return $this->get_taxonomy_terms( 'auto-status' );

    }



    /**

     * Return taxonomy terms

     * @param $taxonomy

     * @return bool|null|string

     */

    public function get_taxonomy_terms( $taxonomy ) {

        if ( !$this->auto_id || !$taxonomy || !taxonomy_exists( $taxonomy ) ) {

            return false;

        }

        $taxonomy_terms = get_the_terms( $this->auto_id, $taxonomy );

        if ( !empty( $taxonomy_terms ) && !is_wp_error( $taxonomy_terms ) ) {

            $terms_count = count( $taxonomy_terms );

            $taxonomy_terms_str = '';

            $loop_count = 1;

            foreach ( $taxonomy_terms as $single_term ) {

                $taxonomy_terms_str .= $single_term->name;

                if ( $loop_count < $terms_count ) {

                    $taxonomy_terms_str .= ', ';

                }

                $loop_count++;

            }

            return $taxonomy_terms_str;

        }

        return null;

    }



    /**

     * Return slug or name of first term of given taxonomy

     * @param $taxonomy

     * @param string $field

     * @return null|string

     */

    public function get_taxonomy_first_term( $taxonomy, $field = 'slug' ) {

        if ( !$this->auto_id || !$taxonomy || !taxonomy_exists( $taxonomy ) ) {

            return null;

        }

        $taxonomy_terms = get_the_terms( $this->auto_id, $taxonomy );

        if ( !empty( $taxonomy_terms ) && !is_wp_error( $taxonomy_terms ) ) {

            foreach ( $taxonomy_terms as $single_term ) {

                if ( $field == 'name' ){

                    return $single_term->name;

                } elseif ( $field == 'slug' ){

                    return $single_term->slug;

                } else {

                    return $single_term;

                }

            }

        }

        return null;

    }



}