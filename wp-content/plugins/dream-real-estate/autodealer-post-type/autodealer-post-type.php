<?php 
/*
	Plugin Name: Autodealer Post Type
	Plugin URI: http://webmasterdriver.net/
	Description: Its Autodealer Post Type slider post type, team post type, testimonial post type, portfolio post type .
	Author: WebMasterDriver Team
	Author URI: http://webmasterdriver.net/
	Donate link: http://webmasterdriver.net/
	Contributors: specialk
	Version: 1.3v
	Text Domain: wmd
	Domain Path: /languages
	License: GPL v2 or later
*/
//************************** Testimonial Post Type ****************************//
add_action( 'init', 'testimonial_post_type' );
function testimonial_post_type() {
    register_post_type( 'testimonial',
       	 array(
            'labels' => array(
				'name'					=> __( 'Testimonial', 'wmd' ),
				'singular_name'			=> __( 'Testimonial Item', 'wmd' ),
				'add_new'				=> __( 'Add New Item', 'wmd' ),
				'add_new_item'			=> __( 'Add New Testimonial Item', 'wmd' ),
				'edit_item'				=> __( 'Edit Testimonial Item', 'wmd' ),
				'new_item'				=> __( 'Add New Testimonial Item', 'wmd' ),
				'view_item'				=> __( 'View Item', 'wmd' ),
				'search_items'			=> __( 'Search Testimonial', 'wmd' ),
				'not_found'				=> __( 'No testimonial items found', 'wmd' ),
				'not_found_in_trash'	=> __( 'No testimonial items found in trash', 'wmd' )
            ),
				'public'			=> true,
				'supports'			=> array( 'title', 'editor', 'thumbnail'),
				'capability_type'	=> 'post',
				'rewrite'			=> array( 'slug' => 'testimonial-item' ),
				'has_archive'		=> false,
				'menu_icon'			=> 'dashicons-format-status',
				'menu_position'		=> 21,
        )		
    );
}

//************************** Team Post Type ***********************//
add_action( 'init', 'team_post_type' );
function team_post_type() {
    register_post_type( 'team',
       	 array(
            'labels' => array(
			'name'					=> __('Team Members', 'autodealer'),
			'singular_name'			=> __('Team Members', 'autodealer'),
			'add_new'				=> __('Add New Team Member', 'autodealer'),
			'add_new_item'			=> __('Add New Team Members', 'autodealer'),
			'edit_item'				=> __('Edit Team Member', 'autodealer'),
			'new_item'				=> __('Add New Team Members', 'autodealer'),
			'view_item'				=> __('View Team Members', 'autodealer'),
			'search_items'			=> __('Search Team Members', 'autodealer'),
			'not_found'				=> __('No Team Members items found', 'autodealer'),
			'not_found_in_trash'	=> __('No Team Members items found in trash', 'autodealer')
            ),
			'public'			=> true,
			'supports'			=> array('title', 'editor','thumbnail'),
			'capability_type'	=> 'post',
			'rewrite'			=> array('slug' => 'team-item'),
			'has_archive'		=> false,
			'menu_icon'			=> 'dashicons-groups',
			'menu_position'		=> 20,
        )		
    );	
}

//************************** Popular Brands Post Type ***********************//
function auto_popular_brands() {
	register_post_type( 'brand',
	// CPT Options
		array(
			'labels' => array(
				'all_items'           => esc_html__('All Popular Brands', 'autodealer'),
				'view_item'           => esc_html__('View Popular Brands', 'autodealer'),
				'add_new_item'        => esc_html__('Add New', 'autodealer'),
				'add_new'             => esc_html__('Add New', 'autodealer'),
				'edit_item'           => esc_html__('Edit Popular Brand', 'autodealer'),
				'update_item'         => esc_html__('Update Popular Brand', 'autodealer'),
				'search_items'        => esc_html__('Search Popular Brands', 'autodealer'),
						
				'name' => esc_html__('Popular Brand', 'autodealer'),
				'singular_name' => esc_html__('Popular Brands', 'autodealer')
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'Popular Brand'),
			'supports' => array( 'title','thumbnail'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'auto_popular_brands');


add_action('admin_head', 'wpds_custom_admin_post_css');
function wpds_custom_admin_post_css() {
    global $post_type;
    if ($post_type == 'testimonial' || $post_type == 'team' || $post_type == 'brand' || $post_type == 'agent') {
        echo "<style>#edit-slug-box {display:none;}</style>";
    }
}

//custom-css
function carforyou_custom_css() {
  echo '<style>
    .updated.redux-message.redux-notice.notice.is-dismissible.redux-notice {display: none;}
	#menu-posts-agent {display: none;}
  </style>';
}
add_action('admin_head', 'carforyou_custom_css');
