<?php
/**
 *
 * @link              http://wordpress.org/themes/
 * @since             1.0.0
 * @package           Dream_Real_Estate
 *
 * @wordpress-plugin
 * Plugin Name:       Dream Real Estate
 * Plugin URI:        http://wordpress.org/plugins/
 * Description:       Dream real estate plugin provides auto post type and agent post type with related functionality.
 * Version:           1.0.1
 * Author:            D S S
 * Author URI:        http://wordpress.org/user/
 * Text Domain:       dream-real-estate
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define( 'DREAM_REAL_ESTATE_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dream-real-estate-activator.php
 */
function activate_dream_real_estate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dream-real-estate-activator.php';
	Dream_Real_Estate_Activator::activate();
}
/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dream-real-estate-deactivator.php
 */
function deactivate_dream_real_estate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dream-real-estate-deactivator.php';
	Dream_Real_Estate_Deactivator::deactivate();
}
register_activation_hook( __FILE__, 'activate_dream_real_estate' );
register_deactivation_hook( __FILE__, 'deactivate_dream_real_estate' );
/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dream-real-estate.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
$dream_real_estate = Dream_Real_Estate::get_instance();
$dream_real_estate->run();
/*
 * Meta Box Extensions
 */
require_once ( plugin_dir_path( __FILE__ ) . 'meta-box-extensions/meta-box-columns/meta-box-columns.php' );         // columns
require_once ( plugin_dir_path( __FILE__ ) . 'meta-box-extensions/meta-box-show-hide/meta-box-show-hide.php' );     // show hid
require_once ( plugin_dir_path( __FILE__ ) . 'meta-box-extensions/meta-box-tabs/meta-box-tabs.php' );               // tabs
//Custom Post Type
require_once ( plugin_dir_path( __FILE__ ) . 'autodealer-post-type/autodealer-post-type.php');
require_once ( plugin_dir_path( __FILE__ ) . 'includes/class-dream-auto-function.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'includes/class-dream-auto-js.php' );
